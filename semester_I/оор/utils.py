class Smartphone:
	def __init__(self, id_, name, time, power):
		self.id_ = int(id_)
		self.name = name
		self.time = int(time)
		self.power = int(power)
	def show(self):
		return '\t'.join(map(str, [self.id_, self.name, self.time, self.power]))
		
def sort_power(obj):
	boo = False
	while not boo:
		boo = True
		for i in range(len(obj) - 1):
			if obj[i].power < obj[i + 1].power:
				obj[i], obj[i + 1] = obj[i + 1], obj[i]
				boo = False

def sort_time(obj):
	boo = False
	while not boo:
		boo = True
		for i in range(len(obj) - 1):
			if obj[i].time < obj[i + 1].time:
				obj[i], obj[i + 1] = obj[i + 1], obj[i]
				boo = False

