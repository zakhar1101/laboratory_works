from turtle import *
import sys

n = int(input('Введите цифру от 0 до 9: '))

win = Screen()
win.bgcolor('green')

p = Turtle()
p.color('red')

const = (100**2+100**2)**(1/2)

def f0(n):
	return p.forward(n)
def f1(n):
	return p.left(n)

p.penup()
p.setpos(-50, 100)
if n == 0:
	p.pendown()
	for i in range(4):
		if i % 2 == 0:
			p.forward(100)
			p.right(90)
		else:
			p.forward(200)
			p.right(90)
elif n == 1:
	p.right(90)
	p.forward(100)
	p.pendown()
	p.right(-135)
	p.forward(100)
	p.right(135)
	p.forward(200)
elif n == 2:
	p.pendown()
	p.forward(100)
	p.right(90)
	p.forward(100)
	p.right(45)
	p.forward(const)
	p.left(135)
	p.forward(100)
elif n == 3:
	p.pendown()
	p.forward(100)
	p.right(135)
	p.forward(const)
	p.right(-135)
	p.forward(100)
	p.right(135)
	p.forward(const)
elif n == 4:
	p.pendown()
	p.right(90)
	f0(100)
	f1(90)
	f0(100)
	f1(90)
	f0(100)
	f1(180)
	f0(200)
elif n == 5:
	p.pendown()
	f0(100) ; f1(180) ; f0(100) ; f1(90) ; f0(100) ; f1(90) ; f0(100) ; f1(-90) ; f0(100) ; f1(-90) ; f0(100)
elif n == 6:
	f0(100)
	f1(-135)
	p.pendown()
	f0(const)
	f1(45)
	for i in range(4):
		f0(100)
		f1(90)
elif n == 7:
	p.pendown()
	f0(100)
	f1(-135)
	f0(const)
	f1(45)
	f0(100)
elif n == 8:
	p.pendown()
	for i in range(4):
		if i % 2 == 0:
			f0(100)
			f1(-90)
		else:
			f0(200)
			f1(-90)
	f1(-90)
	f0(100)
	f1(90)
	f0(100)
elif n == 9:
	p.pendown()
	f1(-90)
	for i in range(4):
		f0(100) ; f1(90)
	f1(90)
	f0(100)
	f1(-90)
	f0(100)
	f1(-45)
	f0(const)










win.mainloop()
