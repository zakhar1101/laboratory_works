from turtle import *





def numb(obj, p1, n, siz):
	siz = int(siz)
	const = (siz**2+siz**2)**(1/2)
	def f0(n):
		return obj.forward(n)
	def f1(n):
		return obj.left(n)


	obj.penup()
	obj.setpos(p1, 100)
	if n == 0:
		obj.pendown()
		for i in range(4):
			if i % 2 == 0:
				obj.forward(siz)
				obj.right(90)
			else:
				obj.forward(2*siz)
				obj.right(90)
	elif n == 1:
		obj.right(90)
		obj.forward(siz)
		obj.pendown()
		obj.right(-135)
		obj.forward(const)
		obj.right(135)
		obj.forward(2*siz)
	elif n == 2:
		obj.pendown()
		obj.forward(siz)
		obj.right(90)
		obj.forward(siz)
		obj.right(45)
		obj.forward(const)
		obj.left(135)
		obj.forward(siz)
	elif n == 3:
		obj.pendown()
		obj.forward(siz)
		obj.right(135)
		obj.forward(const)
		obj.right(-135)
		obj.forward(siz)
		obj.right(135)
		obj.forward(const)
	elif n == 4:
		obj.pendown()
		obj.right(90)
		f0(siz)
		f1(90)
		f0(siz)
		f1(90)
		f0(siz)
		f1(180)
		f0(2*siz)
	elif n == 5:
		obj.pendown()
		f0(siz) ; f1(180) ; f0(siz) ; f1(90) ; f0(siz) ; f1(90) ; f0(siz) ; f1(-90) ; f0(siz) ; f1(-90) ; f0(siz)
	elif n == 6:
		f0(siz)
		f1(-135)
		obj.pendown()
		f0(const)
		f1(45)
		for i in range(4):
			f0(siz)
			f1(90)
	elif n == 7:
		obj.pendown()
		f0(siz)
		f1(-135)
		f0(const)
		f1(45)
		f0(siz)
	elif n == 8:
		obj.pendown()
		for i in range(4):
			if i % 2 == 0:
				f0(siz)
				f1(-90)
			else:
				f0(2*siz)
				f1(-90)
		f1(-90)
		f0(siz)
		f1(90)
		f0(siz)
	elif n == 9:
		obj.pendown()
		f1(-90)
		for i in range(4):
			f0(siz) ; f1(90)
		f1(90)
		f0(siz)
		f1(-90)
		f0(siz)
		f1(-45)
		f0(const)
	
